**What is printf designed to display?**

Format string, with values possibly inserted at specified points in the string.

---

**What is the syntax for printf?**

``` printf(string,expr1,expr2) ```

---

**What is conversion specifications?**

THey begin with % character, which specifies how the value is converted from binary to characters.

%d -> binary to int
%f -> binary to float

---

**What are the various format specifiers?**

d -> displays an integer in decimal form.

i -> integer expressed in octal (base 8),decimal,hexadecimal(base 16)

e -> displays a floating point number in exponential format.

f -> displays floating point number in fixed decimal format.

g -> displays a floating point in either exponential format or fixed decimal point depending on the numbers size. Useful for displays numbers whose values cannot be predicted

---

**Why is & mandatory infront of a variable in a call with scanf?**

Value that is read from the input won't be stored in the variable. Instead variable will retain its old value which maybe meaningless.

---

**How does scanf work?**

It uses pattern matching and skips white-space characters.

---

**Should scanf format strings match that of printf?**

No, in the following example `scanf("%d, %d, &i, &j")`, scanf will look for an integerw ith an input and match it with `,`

---

**Should we use `\n` in scanf?**

No

---
