#include <stdio.h>

int main(void) {
  // TODO finish ex 3
  int gsi_prefix, group_ident, publish_code, item_number, check_digit;
  printf("Enter ISBN: ");
  scanf("%d-%d-%d-%d-%d", &gsi_prefix, &group_ident, &publish_code,
        &item_number, &check_digit);
  printf("GSI Prefix: %d\n", gsi_prefix);
  printf("Group Identifier: %d\n", group_ident);
  printf("Publisher code: %d\n", publish_code);
  printf("Item number: %d\n", item_number);
  printf("Check digit: %d\n", check_digit);
  return 0;
}

