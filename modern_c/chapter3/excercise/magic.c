#include <stdio.h>

int main(void) {
  int a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;
  printf("Enter any 16 digits in any order :");
  scanf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", &a, &b, &c, &d, &e,
        &f, &g, &h, &i, &j, &k, &l, &m, &n, &o, &p);
  printf("%d %d %d %d \n %d %d %d %d\n %d %d %d %d\n %d %d %d %d\n", a, b, c, d,
         e, f, g, h, i, j, k, l, m, n, o, p);
  printf("Row sums %d %d %d %d\n", a + b + c + d, e + f + g + h, i + j + k + l,
         m + n + o + p);
  return 0;
}

