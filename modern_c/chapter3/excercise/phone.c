#include <stdio.h>

int main(void) {
  int phoneNo, phoneMid, phoneEnd;
  printf("Enter phone number [(xxx) xxx-xxxx]: \n");
  scanf("(%d) %d-%d", &phoneNo, &phoneMid, &phoneEnd);
  printf("You entered %d.%d.%d", phoneNo, phoneMid, phoneEnd);
  return 0;
}

