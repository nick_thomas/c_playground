#include <stdio.h>

int main(void) {
  int income;
  float amountDue;
  printf("Enter the income\n");
  scanf("%d", &income);
  if (income < 750) {
    amountDue = income * 0.01;
  } else if (income > 750 && income < 2250) {
    amountDue = 7.50 + (income * 0.02);
  } else if (income > 2250 && income < 3750) {
    amountDue = 37.50 + (income * 0.04);
  } else if (income > 5250 && income < 7000) {
    amountDue = 142.50 + (income * 0.05);
  } else {
    amountDue = 230.00 + (income * 0.06);
  }
  printf("The tax due is %.2f", amountDue);
  return 0;
}

