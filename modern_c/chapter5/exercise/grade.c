#include <stdio.h>

int main(void) {
  int grade = 0;
  printf("Enter the numerical grade\n");
  scanf("%d", &grade);
  int calculate = (grade - 1) / 10;
  switch (calculate) {
  case 9:
    printf("A\n");
    break;
  case 8:
    printf("B\n");
    break;
  case 7:
    printf("C\n");
    break;
  case 6:
  case 5:
    printf("D\n");
    break;
  default:
    printf("F\n");
    break;
  }
  return 0;
}

