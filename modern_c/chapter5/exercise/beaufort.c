#include <stdio.h>

int main(void) {
  int knot;
  printf("Enter the speed in knots\n");
  scanf("%d", &knot);
  if (knot < 1) {

    printf("Calm");
  } else if (knot >= 1 && knot <= 3) {
    printf("Light air");
  } else if (knot >= 4 && knot <= 27) {
    printf("Breeze");
  } else if (knot >= 28 && knot <= 47) {
    printf("Gale");
  } else if (knot >= 48 && knot <= 63) {
    printf("Storm");
  } else if (knot > 63) {
    printf("Hurricane");
  }
  return 0;
}

