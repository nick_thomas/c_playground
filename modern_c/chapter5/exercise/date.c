#include <stdio.h>

int main(void) {
  int date1, month1, year1, date2, month2, year2;
  printf("Enter first date (mm/dd/yy)\n");
  scanf("%2d/%2d/%2d", &date1, &month1, &year1);
  printf("Enter second date (mm/dd/yy)\n");
  scanf("%2d/%2d/%2d", &date2, &month2, &year2);
  if (year1 > year2) {
    printf("%d/%d/%d is earlier than %d/%d/%d", date2, month2, year2, date1,
           month1, year1);
    return 0;
  } else if (year1 == year2) {
    if (month2 > month1) {

      printf("%d/%d/%d is earlier than %d/%d/%d", date1, month1, year1, date2,
             month2, year2);
      return 0;
    } else if (month1 == month2) {
      if (date2 > date1) {

        printf("%d/%d/%d is earlier than %d/%d/%d", date1, month1, year1, date2,
               month2, year2);
        return 0;
      }
    }
  }

  printf("%d/%d/%d is earlier than %d/%d/%d", date2, month1, year1, date2,
         month2, year2);
  printf("%d/%d/%d is earlier than %d/%d/%d", date2, month2, year2, date1,
         month1, year1);

  return 0;
}

