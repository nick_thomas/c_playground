#include <stdio.h>
#include <stdlib.h>

struct Flight {
  int departure;
  int arrival;
  char departure_string[12];
  char arrival_string[12];
};

struct Flight* find_closest_deparature(struct Flight flight[],int size, int time){
  int closest_index = 0;
  int min_diff = abs(flight[0].departure-time);
  for(int i=1;i< size;i++){
    int diff = abs(flight[i].departure - time);
    if (diff < min_diff){
      min_diff = diff;
      closest_index = i;
    }
  }
  return &flight[closest_index];
}

int main(void) {
  int time,hour,seconds;
  struct Flight flight[8] = {
    {480, 616, "8:00 a.m.", "10:16 a.m."},
    {583, 712,"9:43 a.m.","11:52 a.m."},
    {679, 811,"11:19 a.m.","1:31 p.m."},
    {767, 900, "12:47 p.m.","3:00 p.m."},
    {840, 968, "2:00 p.m.","4:08 p.m."},
    {945, 1075, "3:45 p.m.", "5:55 p.m."},
    {1140, 1280, "7:00 p.m.","9:20 p.m."},
    {1305, 1438, "9:45 p.m.", "11:58 p.m."}
  };
  printf("Enter a 24-hour time:\n");
  scanf("%d:%d",&hour,&seconds);
  time = hour * 60 + seconds;
  struct Flight* closest_flight = find_closest_deparature(flight, 8,time);
  printf("Closest flight is departing at %s & arriving at %s\n",closest_flight->departure_string,closest_flight->arrival_string);
  return 0;
}

