#include <stdio.h>

int main(void) {
  int d1, d2, d3, d4;
  int largest, smallest;
  printf("Enter four integers \n");
  scanf("%d%d%d%d", &d1, &d2, &d3, &d4);
  if (d1 > d2 && d1 > d3 && d1 > d4) {
    largest = d1;
    smallest = d2;
  }
  if (d2 > d3 && d2 > d4 && d2 > d1) {
    largest = d2;
    smallest = d3;
  }
  if (d3 > d4 && d3 > d1 && d3 > d2) {
    largest = d3;
    smallest = d4;
  }
  if (d4 > d1 && d4 > d2 && d4 > d3) {
    largest = d4;
    smallest = d1;
  }
  printf("Largest and smallest %d,%d", largest, smallest);
  return 0;
}

