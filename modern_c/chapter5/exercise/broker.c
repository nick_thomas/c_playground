#include <stdio.h>

// Modify the broker. c program of Section 5.2 by making both of the following
// changes:
//(a) Ask the user to enter the number of shares and the price per share,
// instead of the value of the trade. (b) Add statements that compute the
// commission charged by a rival broker ($33 plus 3 € per share for fewer than
// 2000 shares: $33 plus 2e per share for 2000 shares or more). Dis- play the
// rival's commission as well as the commission charged by the original broker.

int main(void) {
  float commision, value, shares, price, rival_commision;
  printf("Enter the value,no of shares and price per share: ");
  scanf("%f %f", &shares, &price);
  if (shares < 2000) {
    rival_commision = 33 + (3 * shares * price);
  } else {
    rival_commision = 33 + (2 * shares * price);
  }
  value = shares * price;
  if (value < 2500.00f)
    commision = 30.00f + .017f * value;
  else if (value < 6250.00f)
    commision = 56.00f + .0066f * value;
  else if (value < 20000.00f)
    commision = 76.00f + .0034f * value;
  else if (value < 50000.00f)
    commision = 100.00f + .0022f * value;
  else if (value < 500000.00f)
    commision = 155.00f + 0.0011f * value;
  else
    commision = 255.00f + .0009f * value;

  if (commision < 39.00f)
    commision = 39.00f;
  printf("Commission: $%.2f\n", commision);
  printf("Rival Commission: $%.2f\n", rival_commision);
  return 0;
}

