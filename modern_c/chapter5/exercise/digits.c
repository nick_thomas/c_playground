// Write a program that calculates how many digits a number contains:
// E n t e r a n u m b e r : 3 7 4
// The number 374 h a s 3 d i g i t s
// You may assume that the number has no more than four digits. Hint: Use i f
// statements to test the number: For example, if the number is between 0 and 9,
// it has one digit. If the num- ber is between 10 and 99, it has two digits.

#include <stdio.h>
int main(void) {
  int number;
  printf("Enter a number\n");
  scanf("%d", &number);
  if (number >= 0 && number < 10) {
    printf("Number has one digit");
  } else if (number > 10 && number < 100) {
    printf("Number has two digits");
  } else {
    printf("Number has three digits");
  }
  return 0;
}

