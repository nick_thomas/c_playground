#include <stdio.h>

char *day_time(int value) {
  if (value == 0) {
    return "AM";
  }
  return "PM";
}
int main(void) {
  int hour, second;
  int am = 0;
  printf("Enter a 24 hour time:");
  scanf("%2d:%2d", &hour, &second);
  if (hour > 12) {
    hour = hour - 12;
    am = 1;
  }
  printf("\nEquivalent 12 hour time: %02d:%02d %s", hour, second, day_time(am));
  return 0;
}

