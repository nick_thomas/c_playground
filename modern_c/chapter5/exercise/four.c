#include <stdio.h>

int main(void) {
  int i, j;
  printf("Enter either two nums");
  scanf("%d%d", &i, &j);
  int result = i > j ? 1 : ((i < j) ? -1 : 0);
  printf("The value is %d", result);
  return 0;
}

