**What is the dangling else problem?**

C follows the rule than else clause belongs to the nearest if statement.
```c
if(y!=0)
    if (x !=0)
        result x/y
else
    printf("Error: y is equal to 0\n")
```
In the above else belongs to nested if block

---

**What is conditional operator?**

It is a ternary operator, where expr 1 is evaluated first, if its value isnt zero then expr 2 is evaluated. Otherwize expr 3 is evaluated.

```c
expr1 ? expr2 : expr 3
```

---

**Why do we need break statement for switch?**

Switch is a form of "computed jump", when the controlling expression is evaluated, control jumps to the case label matching the value of the switch expression.To avoid this we use break

---
