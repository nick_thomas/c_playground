**What does the unary operator do?**

Unary operators require one operand and emphasize that a numeric constant is positive/negative

---

**What is implementation defined behaviour?**

Part of C's philosophy to leave parts of language unspecified for hardware optomization

---

**What is the operator precedence heirarchy?**

Highest: + - (unary)
         * / %
Lowest: +  - (binary)

---

**Is assignment a statement or an operator in C?**

Unlike other languages in C assignment is an operator. The value of an assignment v = e is the value of v after assignment

---

**Can several assignments be chained together?**

Since it is an operator
```c
i = j = k = 0; // i = (j = (k = 0))
```

---

**What is an *lvalue*?**

Assignment (=) operator requires an lvalue as it is left operator. It represents an object stored in computer memory.
