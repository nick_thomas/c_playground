#include <stdio.h>

int main(void) {
  int a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13;
  printf("Enter the first 12 digits of an EAN ");
  scanf("%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d", &a1, &a2, &a3, &a4, &a5, &a6,
        &a7, &a8, &a9, &a10, &a11, &a12);
  printf("Check digit is");
  scanf("%1d", &a13);
  int total = a2 + a4 + a6 + a8 + a10 + a12;
  int secondTotal = a1 + a3 + a5 + a7 + a9 + a11;
  total *= 3;
  total += secondTotal;
  total -= 1;
  total %= 10;
  total = 9 - total;
  if (a13 == total) {
    printf("Valid");
  } else {
    printf("Invalid");
  }
  return 0;
}

