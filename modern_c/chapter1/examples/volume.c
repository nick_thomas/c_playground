#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
  int height,length,width,volume,weight;
  if (argc != 4){
    printf("Needs height,length and width to be supplied as integers for %s to run",argv[0]);
    return 1;
  }
  char *p;
  height = strtol(argv[1],&p,10);
  length = strtol(argv[2],&p,10);
  width = strtol(argv[3],&p,10);
  volume = height * length * width;
  weight = (volume + 165) / 166;
  printf("Dimensions: %dx%dx%dx\n",length,width,height);
  printf("Volume (cubic inches): %d\n",volume);
  printf("Dimensional weight (pounds): %d\n",weight);
  return 0;
}

