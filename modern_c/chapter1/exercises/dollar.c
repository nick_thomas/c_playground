#include<stdio.h>

int main(void){
  int dollar;
  printf("Enter a dollar amount:");
  scanf("%d",&dollar);
  int values[4] = {20,10,5,1};
  int size = sizeof(values) / sizeof(values[0]);
  for(int i =0; i< size;i++){
    int value = dollar / values[i];
    dollar = dollar % values[i];
    printf("$%d bills: %d\n",values[i],value);
  }
  return 0;
}

