#include <stdio.h>

const char *digitToText(int digit) {
  if (digit == 0) {
    return "first";
  }
  if (digit == 1) {
    return "second";
  }
  if (digit == 2) {
    return "third";
  }
  return "unknown";
}

int main(void) {
  float amount, interest, monthly;
  printf("Enter amount of loan:");
  scanf("%f", &amount);
  printf("\nEnter interest rate:");
  scanf("%f", &interest);
  printf("\nEnter monthly payment:");
  scanf("%f", &monthly);
  for (int i = 0; i < 3; i++) {

    float interestRate = interest / 12;
    amount += (amount * interestRate) / 100;
    amount -= monthly;
    printf("\nBalance remaining after %s payment: %.2f", digitToText(i),
           amount);
  }
  return 0;
}

