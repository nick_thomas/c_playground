
#include <stdio.h>
#define INCHES_PER_POUND 166
#define HEIGHT 166
#define WEIGHT 166
#define WIDTH 166
int main(void){
  int volume;
  volume = HEIGHT * WIDTH * WEIGHT;
  printf("Volume (cubic inches): %d\n",volume);
  printf("Dimensional weight (pounds): %d\n",volume + INCHES_PER_POUND - 1 / INCHES_PER_POUND);
  return 0;

}

