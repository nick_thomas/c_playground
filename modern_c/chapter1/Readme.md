**What is a preprocessor?**

Before C program is compiled, it is edited by preprocessor.

---

**What are preprocessor directives?**

Commands intended for preprocessor are called directives. For example `#include <stdio.h>`

---

**What are the 2 types of functions?**

One written by programmer and other provided by C library.

---

**Why is main special?**

It gets called automatically when the program is executed.

---

**Does main function need to return a value?**

Yes a status code that is given to the operating system when the program terminates.

---

**What is a statement?**

It is a command to be executed.

---

**How to assign variables?**


It must be declared first.

```
int height;
height= 8;
```
---

**How to assign a float?**

```
profit = 2150.48f
```

---

**What are the format characters for printing int an float**?

For int we use `%d` and for float we use `%f`

---

**What is the default number of digits after decimal shown by %f?**

6

---

**How to force %f to display n number of digits after the decimal point?**

`%.nf` where n = number of digits we want to print

---

**What is argv[0]?**

It is the program name

---

**Can printf display the value of numeric expression?**

Yes. `volume = height * length * width; printf("%d\n",volume)` is same as `printf("%d\n",height * length * width);`

---

**What is scanf?**

It allows us to obtain input in a formatted way using *format* string.

---

**What is Macro definition?**

Macro definition uses a define preprocessing directive to define constants. It can also be an expression

`#define INCHES_PER_POUND 166`
`#define RECIPROCAL_OF_PI (1.0f/3.141f)`

---
