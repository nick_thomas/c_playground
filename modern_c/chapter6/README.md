Page no 111
## Loops

**What is the syntax for while loop?**

```c
while (i<n)
    i = i* 2
```

_Note_: Parentheses are mandatory and nothing goes between the right parenthesis and the loop body.
_Note_: value between parenthesis is evaluated to be non zero and loop body is executed

---

**How to break out of infinite loop?**

By transfering control using _break_,_goto_ or _return_ calls

---

**Howto write for loops?**

```
for { expr1; expr 2 ; expr3} statement
```

---

**What is the Comma operator?**

It allows us to initialize more than one expression in expr1 or expr3 of a for loop
