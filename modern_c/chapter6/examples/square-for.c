#include <stdio.h>

int main(void) {
  int i, n, k;
  printf("This program prints a table of squares.\n");
  printf("Enter number of entries in table: ");
  scanf("%d", &n);
  for (i = 1, k = 10; i <= n; i++, k *= i) {
    printf("%10d%10d%10d\n", i, i * i, k);
  }
  return 0;
}

