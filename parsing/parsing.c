#include <stdio.h>
#include <stdlib.h>
#include "mpc.h"

/* if we are compiling on Windows compile these functions*/
#ifdef _WIN32
#include <string.h>

static char buffer[2048];

/* Fake readline function */
char* readline(char* prompt){
	fputs(prompt,stdout);
	fgets(buffer,2048,stdin);

	char* cpy = malloc(strlen(buffer)+1);
	strcpy(copy,buffer);

	cpy[strlen(cpy)-1] = '\0';

	return cpy;
	}

/* Fake add_history function */

void add_history(char* unused){}

/* Otherwise include the editline headers */

#else

#include <editline/readline.h>
#endif

/* Declare a buffer for user input of size 2048 */

/* Create some parsers */
mpc_parser_t* Number = mpc_new("number");

mpc_parser_t* Operator = mpc_new("operator");

mpc_parser_t* Expr = mpc_new("expr");

mpc_parser_t* Lispy = mpc_new("lispy");

/* Define them with the following Language */
mpca_lang(MPCA_LANG_DEFAULT,
		"                                              \
		number   :/-?[0-9]+/;                           \
		operator : '+' | '-' | '*' | '/'                 \
		expr     : <number> | '(' <operator><expr>+ ')' ; \
		lispy    : /^/ <operator><expr>+ /$/ ;            \
		", Number, Operator, Expr, Lispy);

int main(int argc, char** argv){
	/* Print Version and Exit information */
  mpc_cleanup(4,Number,Operator,Expr, Lispy);
	puts("Lispy Version 0.0.0.0.1");
	puts("Press Ctrl+c to Exit\n");

	/*  In a never ending loop */
	while(1){
		char* input = readline(" lispy> ");

		/* Add input to history */
		add_history(input);


		/* Echo input back to user */

		printf("No way %c",input);

		/* Free retreived input */
		free(input);
	}

	return 0;
}

