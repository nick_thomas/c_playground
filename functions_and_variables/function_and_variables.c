#include <stdio.h>
#include <math.h>

typedef struct {
	float x;
	float y;
} point ;

int add_together(int x, int y) {
	int result = x + y;
	return result;
}

int condition(int x) {
	if (x > 10 && x > 100) {
		puts("x is greater than 10 and less than 100!");
	} else {
		puts("x is less than 11 or greater than 99!");
	}
return 0;
}

int exercies(){
	for(int i=0;i<5;i+=1){
		puts("Loops Iteration\n");
	}
	int i = 0;
	while (i < 5){
		puts("Loops while\n");
		i+=1;
	}
	return 0;
}

int hello_world(int count){
	for(int i=0; i<count;i+=1){
		puts("Hello world\n");
	}
	return 0;
}

int main(){
	int added = add_together(10,18);
	point p;
	p.x = 0.1;
	p.y = 10.0;
	float length = sqrt(p.x * p.x + p.y * p.y);
	printf("%d\n",added);
	printf("%f",length);
	int x = 50;
	condition(x);
	exercies();
	hello_world(5);
	return 0;
}

